const initState = {
    subject: {
        isLoaded: false,
        name: ''
    },
    group: {
        isLoaded: false,
        name: ''
    },
    auth: {
        isAuth: false,
        teacher: {
            id: '',
            name: '',
            secondName: '',
            login: ''
        }
    },
    alert: {
        show: false,
        event: '',
        text: ''
    },
    disciplines: [],
    workDisciplines: [],
    groups: [],
    individualTasksURL: '',
}

export const reducer = (state = initState, action) => {
    switch (action.type) {
        case 'UPLOAD_FILE_SUCCESS':
            return {
                ...state,
                subject: {
                    ...state.subject,
                    isLoaded: true,
                    name: action.name
                }
            }

        case 'UPLOAD_GROUP_SUCCESS':
            return {
                ...state,
                group: {
                    ...state.group,
                    isLoaded: true,
                    name: action.name
                }
            }

        case 'LOGIN_SUCCESS':
            return {
                ...state,
                auth: {
                    ...state.auth,
                    isAuth: true,
                    teacher: action.teacher
                }
            }

        case 'SHOW_ALERT': {
            return {
                ...state,
                alert: {
                    ...state.alert,
                    show: true,
                    event: action.event,
                    text: action.text
                }
            }
        }

        case 'CLOSE_ALERT': {
            return {
                ...state,
                alert: {
                    ...state.alert,
                    show: false
                }
            }
        }

        case 'SET_DISCIPLINES': {
            return {
                ...state,
                disciplines: action.disciplines
            }
        }

        case 'SET_GROUPS': {
            return {
                ...state,
                groups: action.groups
            }
        }

        case 'SET_WORK_DISCIPLINES': {
            return {
                ...state,
                workDisciplines: action.disciplines
            }
        }

        case 'ADD_WORK_DISCIPLINE': {
            return {
                ...state,
                workDisciplines: [...state.workDisciplines, action.discipline]
            }
        }

        case 'DOWNLOAD_INDIVIDUAL_TASKS': {
            return {
                ...state,
                individualTasksURL: action.url
            }
        }

        default:
            return state;
    }
}