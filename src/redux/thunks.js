import {
    authorization, completeTask, createWorkDiscipline, downloadIndividualWorks, failedTask,
    getDisciplinesByTeacherId, getWorkDisciplinesByTeacher,
    getWorkGroups,
    registration,
    uploadGroupFile,
    uploadSubjectFile
} from "../service/service";
import { sha512 } from "crypto-hash";

const setTimeoutCloseAlert = () => (dispatch) => {
    setTimeout(() => {
        dispatch({ type: 'CLOSE_ALERT' });
    }, 2000);
}

export const getDisciplines = (teacherId) => async (dispatch) => {
    try {
        const response = await getDisciplinesByTeacherId(teacherId);

        if (response.status === 200) {
            const disciplines = response.data;
            dispatch({ type: 'SET_DISCIPLINES', disciplines });
        }
    } catch (err) {
        console.log(err);
    }
}

export const getWorkDisciplines = (teacherId) => async (dispatch) => {
    try {
        const response = await getWorkDisciplinesByTeacher(teacherId);

        if (response.status === 200) {
            const disciplines = response.data;
            dispatch({ type: 'SET_WORK_DISCIPLINES', disciplines });
        }
    } catch (err) {
        console.log(err);
    }
}

export const getGroups = (teacherId) => async (dispatch) => {
    try {
        const response = await getWorkGroups(teacherId);

        if (response.status === 200) {
            const groups = response.data;
            dispatch({ type: 'SET_GROUPS', groups });
        }
    } catch (err) {
        console.log(err);
    }
}

export const handleUploadSubjectFile = (file, teacherId) => async (dispatch) => {
    try {
        const data = new FormData();
        data.append('file', file);
        const status = await uploadSubjectFile(data, teacherId);

        if (status) {
            dispatch({ type: 'UPLOAD_FILE_SUCCESS', name: file.name });
        }
    } catch (err) {
        console.error(err);
    }
}

export const handleUploadGroupFile = (file, teacherId) => async (dispatch) => {
    try {
        const data = new FormData();
        data.append('file', file);
        const status = await uploadGroupFile(data, teacherId);

        if (status) {
            dispatch({ type: 'UPLOAD_GROUP_SUCCESS', name: file.name });
        }
    } catch (err) {
        console.error(err);
    }
}

// Регистрация
export const handleRegistration = ({ name, secondName, login, password }) => async (dispatch) => {
    try {
        const response = await registration({ name, secondName, login, password });

        if (response.status === 200) {
            dispatch({ type: 'LOGIN_SUCCESS', teacher: response.data });
            dispatch({ type: 'SHOW_ALERT', event: 'success', text: 'Регистрация прошла успешно!' });
            dispatch(setTimeoutCloseAlert());
            return true;
        } else {
            dispatch({ type: 'SHOW_ALERT', event: 'error', text: 'Ошибка регистрации!' });
            dispatch(setTimeoutCloseAlert());
            return;
        }
    } catch (err) {
        console.error(err);
        dispatch({ type: 'SHOW_ALERT', event: 'error', text: 'Ошибка регистрации!' });
        dispatch(setTimeoutCloseAlert());
    }
}

// Авторизация
export const handleLogin = ({ login, password }) => async (dispatch) => {
    try {
        const passwordHash = await sha512(password);
        const response = await authorization({ login, passwordHash });

        if (response.status === 200) {
            dispatch({ type: 'LOGIN_SUCCESS', teacher: response.data });
            dispatch({ type: 'SHOW_ALERT', event: 'success', text: 'Авторизация прошла успешно!' });
            dispatch(setTimeoutCloseAlert());
            return true;
        } else {
            dispatch({ type: 'SHOW_ALERT', event: 'error', text: 'Ошибка авторизации!' });
            dispatch(setTimeoutCloseAlert());
            return;
        }
    } catch (err) {
        console.error(err);
        dispatch({ type: 'SHOW_ALERT', event: 'error', text: 'Ошибка авторизации!' });
        dispatch(setTimeoutCloseAlert());
    }
}

export const getIndividualTasks = (workDisciplineId) => async (dispatch) => {
    try {
        const response = await downloadIndividualWorks(workDisciplineId);
        const objectURL = URL.createObjectURL(response.data);
        dispatch({ type: 'DOWNLOAD_INDIVIDUAL_TASKS', url: objectURL });
    } catch (err) {
        console.error(err);
    }
}

export const setCompleteTask = (taskIds) => async (dispatch, getState) => {
    try {
        const teacherId = getState()?.auth?.teacher?.id;

        const response = await completeTask(taskIds);
        if (response.status === 200) {
            dispatch(getWorkDisciplines(teacherId));
        }
    } catch (err) {
        console.error(err);
    }
}

export const setFailedTask = (taskIds) => async (dispatch, getState) => {
    try {
        const teacherId = getState()?.auth?.teacher?.id;

        const response = await failedTask(taskIds);
        if (response.status === 200) {
            dispatch(getWorkDisciplines(teacherId));
        }
    } catch (err) {
        console.error(err);
    }
}

export const createWorkDisciplineThunk = ({ teacherId, disciplineId, groupId, workSemester }) => async (dispatch) => {
    try {
        const response = await createWorkDiscipline({ teacherId, disciplineId, groupId, workSemester });
        if (response.status === 200) {
            dispatch({ type: 'ADD_WORK_DISCIPLINE', discipline: response.data });
        }
    } catch (err) {
        console.error(err);
    }
}