import React, { useEffect, useState } from 'react';
import { Box, Button, FormControl, InputLabel, MenuItem, Modal, Select, TextField, Typography } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { createWorkDisciplineThunk, getDisciplines, getGroups } from "../redux/thunks";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const initState = {
    disciplineId: '',
    groupId: '',
    workSemester: ''
}

const DisciplineModal = ({ openModal, setOpenModal }) => {
    const dispatch = useDispatch();

    const [form, setForm] = useState(initState);

    const disciplines = useSelector(state => state.disciplines);
    const groups = useSelector(state => state.groups);
    const auth = useSelector(state => state.auth);

    useEffect(() => {
        if (openModal) {
            dispatch(getDisciplines(auth.teacher.id));
            dispatch(getGroups(auth.teacher.id));
        }
    }, [openModal]);


    const handleChange = (type) => (event) => {
        switch (type) {
            case 'discipline':
                setForm((prevState) => ({
                    ...prevState,
                    disciplineId: event.target.value
                }));
                break;

            case 'group':
                setForm((prevState) => ({
                    ...prevState,
                    groupId: event.target.value
                }));
                break;

            case 'workSemester':
                setForm((prevState) => ({
                    ...prevState,
                    workSemester: event.target.value
                }));
                break;

            default:
                break;
        }
    }

    const handleSendForm = () => {
        dispatch(createWorkDisciplineThunk({
            teacherId: auth.teacher.id,
            disciplineId: form.disciplineId,
            groupId: form.groupId,
            workSemester: form.workSemester
        }));

        setOpenModal(false);
        setForm(initState)
    }

    return (
        <Modal
            open={openModal}
            onClose={() => setOpenModal(false)}
        >
            <Box sx={style}>
                <Typography variant="h6" component="h2" sx={{ marginBottom: '20px' }}>
                    Создание рабочей дисциплины
                </Typography>

                <FormControl fullWidth sx={{ margin: '10px 0' }}>
                    <InputLabel>Выбор дисциплины</InputLabel>
                    <Select
                        value={disciplines?.find(item => item.id === form.disciplineId)?.name}
                        label="Выбор дисциплины"
                        onChange={handleChange('discipline')}
                    >
                        {
                            disciplines?.map(item => (
                                <MenuItem value={item.id}>{item.name}</MenuItem>
                            ))
                        }
                    </Select>
                </FormControl>

                <FormControl fullWidth sx={{ margin: '10px 0' }}>
                    <InputLabel>Выбор группы</InputLabel>
                    <Select
                        value={groups?.find(item => item.id === form.groupId)?.name}
                        label="Выбор группы"
                        onChange={handleChange('group')}
                    >
                        {
                            groups?.map(item => (
                                <MenuItem value={item.id}>{item.name}</MenuItem>
                            ))
                        }
                    </Select>
                </FormControl>

                <TextField
                    label="Учебный поток"
                    fullWidth
                    variant="outlined"
                    sx={{ marginTop: '10px' }}
                    value={form.workSemester}
                    onChange={handleChange('workSemester')}
                />

                <Button
                    variant="contained"
                    onClick={handleSendForm}
                    sx={{ marginTop: '20px' }}
                >
                    Сохранить
                </Button>
            </Box>
        </Modal>
    );
};

export default DisciplineModal;
