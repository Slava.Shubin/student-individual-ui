import React, { useState } from 'react';
import { Button, Paper, TextField } from "@mui/material";
import './../css/registration.css';
import { useNavigate } from "react-router";
import { handleRegistration } from "../redux/thunks";
import { useDispatch } from "react-redux";

const Registration = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const [form, setForm] = useState({ name: '', secondName: '', login: '', password: '' });

    const isValidForm = !!form.name && !!form.secondName && !!form.login && !!form.password;

    const onClickRegisterButton = () => {
        dispatch(handleRegistration(form));
    }

    const handleChange = (event) => {
        const { name, value } = event.target;
        setForm(prevState => ({ ...prevState, [name]: value }));
    }

    return (
        <Paper elevation={3} className="registration-form">
            <h3>Регистрация</h3>

            <TextField
                variant="outlined"
                label="Имя"
                name="name"
                fullWidth
                sx={{ marginBottom: '15px' }}
                value={form.name}
                onChange={handleChange}
            />
            <TextField
                variant="outlined"
                label="Фамилия"
                name="secondName"
                fullWidth
                sx={{ marginBottom: '15px' }}
                value={form.secondName}
                onChange={handleChange}
            />
            <TextField
                variant="outlined"
                label="Логин"
                name="login"
                fullWidth
                sx={{ marginBottom: '15px' }}
                value={form.login}
                onChange={handleChange}
            />
            <TextField
                variant="outlined"
                type="password"
                label="Пароль"
                name="password"
                fullWidth
                sx={{ marginBottom: '15px' }}
                value={form.password}
                onChange={handleChange}
            />
            <p className="has-account" onClick={() => navigate('/login')}>Уже есть аккаунт</p>

            <Button
                variant="contained"
                sx={{ margin: '15px 0' }}
                onClick={onClickRegisterButton}
                disabled={!isValidForm}
            >
                Зарегистрироваться
            </Button>
        </Paper>
    );
};

export default Registration;
